FROM ubuntu:20.04
RUN apt-get update && apt-get install -y python3 python3-pip
RUN pip3 install matplotlib
RUN pip3 install scipy
RUN pip3 install numpy
RUN mkdir -p /code
COPY plot.py /code
WORKDIR /code
